import {
  Alert,
  Balancer, ButtonLoading, Input, InputPassword, Label,
  Layout, RemixForm, RemixLinkText
} from "~/components"
import { createMetaData, createSitemap, useRedirectTo } from "~/utils"

import { Outlet, useActionData, useLoaderData, useNavigation } from "@remix-run/react"

import RobotControl from "~/components/robot/RobotControl"
import { TrpcProvider } from "~/context/TrpcContext"
import { LogProvider } from "~/context/LogContext"

export const meta = createMetaData({
  title: "Robot",
  description:
    "meta robot description",
});

export const handle = createSitemap("/robot", 0.9);

// TODO: Load content from HTML/MDX data
export default function Route() {



  return (
    <Layout className="contain-sm space-y-20">
      <article className="prose-config mt-10">
        <TrpcProvider>
          <LogProvider>
            <RobotControl/>
            <Outlet/>
          </LogProvider>
        </TrpcProvider>
      </article>
    </Layout>
  );
}
