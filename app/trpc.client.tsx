import {
    createTRPCProxyClient,
    createWSClient,
    httpLink, loggerLink,
    splitLink,
    wsLink,
} from '@trpc/client';

// import ws from 'ws';
import type { AppRouter } from './trpc.server';
import type {inferProcedureOutput} from "@trpc/server";

// globalThis.WebSocket = ws as any;

const wsClient = createWSClient({
    url: `ws://localhost:2022`,
});

export const trpc = createTRPCProxyClient<AppRouter>({
    links: [
        wsLink({client: wsClient}),
        // call subscriptions through websockets and the rest over http
        // splitLink({
        //     condition(op) {
        //         return op.type === 'subscription';
        //     },
        //     true: wsLink({
        //         client: wsClient,
        //     }),
        //     false: httpLink({
        //         url: `http://localhost:2022`,
        //     }),
        // }),
        loggerLink({
            enabled: (opts) => true
        })
    ],
});

console.log('trpc client loaded', trpc)



export type TRPC = typeof trpc

/**
 * This is a helper method to infer the output of a query resolver
 * @example type HelloOutput = inferQueryOutput<'hello'>
 */
export type inferQueryOutput<
    TRouteKey extends keyof AppRouter['_def']['queries'],
> = inferProcedureOutput<AppRouter['_def']['queries'][TRouteKey]>;