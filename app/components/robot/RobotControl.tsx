// app/routes/RobotControl.tsx

import { Outlet, useActionData, useNavigation } from "@remix-run/react"
import { Alert, Balancer, ButtonLoading, Input, Label, RemixForm, TextArea } from "~/components"
import { conform, useForm } from "@conform-to/react"
import { action } from "~/routes/_auth.login"
import { useId } from "react"
import { z } from "zod"
import { getFieldsetConstraint, parse } from "@conform-to/zod"
import { useLog } from "~/context/LogContext"


type CommandFormData = {
  command: string;
};

export default function RobotControl() {
  const navigation = useNavigation();
  const isSubmitting = navigation.state === "submitting";

  const actionData = useActionData<typeof action>();
  const log = useLog()



  const schemaRobot = z.object({
    command:  z.string().min(1, "command is required")
  });

  return (
    <div>
      <h1>
        <Balancer>Robot Control</Balancer>
      </h1>

      <RemixForm  method="POST" className="space-y-4">
        <fieldset
          className="space-y-2 disabled:opacity-80"
          disabled={isSubmitting}
        >
          <div className="space-y-1">
            <Label htmlFor="command">Command</Label>
            <Input
              id={"command"}
              type="text"
              placeholder="help"
              autoFocus
              required
            />
            {/*{command.error && (*/}
            {/*  <Alert variant="danger" id={command.errorId}>*/}
            {/*    {command.error}*/}
            {/*  </Alert>*/}
            {/*)}*/}
          </div>



          <ButtonLoading
            size="lg"
            type="submit"
            className="w-full"
            name="intent"
            value="submit"
            isSubmitting={isSubmitting}
            loadingText="Sending Command..."
          >
            Send
          </ButtonLoading>
        </fieldset>


      </RemixForm>
      <Label htmlFor="log"/>
      <TextArea id="log" value={log} readOnly={true}/>
    </div>
  );
}
