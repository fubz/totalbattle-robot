/**
 * EDITME: Site Config and Meta Config
 *
 * Site-wide info and meta data, mostly for information and SEO purpose
 */

import { configDev } from "~/configs";

// For general
export const configSite = {
  domain: configDev.isDevelopment
    ? "localhost:3000"
    : "rewinds.mhaidarhanif.com",

  slug: "tb-bot",
  name: "TotalBattle Bot",
  title: "Total Battle: Robot",
  description:
    "Total Battle: Robot plays the game for you",

  links: {
    website: "https://mhaidarhanif.com",
    github: "https://github.com/mhaidarhanif/rewinds",
    twitter: "https://twitter.com/mhaidarhanif",
    youtube: "https://youtube.com/mhaidarhanif",
    facebook: "https://facebook.com/mhaidarhanif",
    instagram: "https://instagram.com/mhaidarhanif_",
    devTo: "https://dev.to/mhaidarhanif",
    hashnode: "https://hashnode.com/mhaidarhanif",
    showwcase: "https://showwcase.com/mhaidarhanif",
  },

  twitter: {
    site: "@fubz",
    creator: "@fubz",
  },

  navItems: [
    { to: "/", name: "Home", icon: "home" },
    { to: "/robot", name: "Robot", icon: "material-symbols:robot-2" },
    { to: "/stats", name: "Stats", icon: "material-symbols:query-stats" },
    { to: "/log", name: "Log", icon: "streamline:programming-script-2-language-programming-code" },
    // { to: "/about", name: "About", icon: "about" },
    // { to: "/components", name: "Components", icon: "components" },
    // { to: "/demo", name: "Demo", icon: "demo" },
    // { to: "/notes", name: "Notes", icon: "notes" },
  ],
};

// For Remix meta function
export const configMeta = {
  defaultName: configSite?.name,
  defaultTitle: configSite?.title,
  defaultTitleSeparator: "—",
  defaultDescription: configSite?.description,

  locale: "en_US",
  url: configDev.isDevelopment
    ? "http://localhost:3000"
    : `https://${configSite?.domain}`,
  canonicalPath: "/",
  color: "#3399cc", // EDITME
  ogType: "website",
  ogImageAlt: configSite?.title,
  ogImageType: "image/png",
  ogImagePath: "/assets/opengraph/rewinds-og.png",
  twitterImagePath: "/assets/opengraph/rewinds-og.png",
  fbAppId: "",

  author: {
    name: "Michael Ski",
    handle: "@fubz",
    url: "https://michael.ski",
    company: {
      name: "fubz",
      handle: "@fubz",
      url: "https://fubz.dev",
    },
  },

  mailingListName: "Unused Mailing List",
};
