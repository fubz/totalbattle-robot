import type { inferAsyncReturnType} from '@trpc/server';
import { initTRPC } from '@trpc/server';
import type {
    CreateHTTPContextOptions} from '@trpc/server/adapters/standalone';
import {
    createHTTPServer,
} from '@trpc/server/adapters/standalone';
import type {
    CreateWSSContextFnOptions} from '@trpc/server/adapters/ws';
import {
    applyWSSHandler
} from '@trpc/server/adapters/ws';
import { observable } from '@trpc/server/observable';
import { WebSocketServer } from 'ws';
import { z } from 'zod';
import {EventEmitter} from "events";

// Robot Events
const robotEE = new EventEmitter()


// This is how you initialize a context for the server
export function createContext(
    opts: CreateHTTPContextOptions | CreateWSSContextFnOptions,
) {
    return {};
}
type Context = inferAsyncReturnType<typeof createContext>;

const t = initTRPC.context<Context>().create();

const publicProcedure = t.procedure;
const router = t.router;

const greetingRouter = router({
    hello: publicProcedure
        .input(
            z.object({
                name: z.string(),
            }),
        )
        .query(({ input }) => `Hello, ${input.name}!`),
});

const postRouter = router({
    createPost: publicProcedure
        .input(
            z.object({
                title: z.string(),
                text: z.string(),
            }),
        )
        .mutation(({ input }) => {
            // imagine db call here
            return {
                id: `${Math.random()}`,
                ...input,
            };
        }),
    randomNumber: publicProcedure.subscription(() => {
        return observable<{ randomNumber: number }>((emit) => {
            const timer = setInterval(() => {
                // emits a number every second
                emit.next({ randomNumber: Math.random() });
            }, 200);

            return () => {
                clearInterval(timer);
            };
        });
    }),
});

const robotRouter = router({
    onLog: publicProcedure.subscription(() => {
        return observable<string>((emit) => {

            console.log('onLog has been created')
            const onLog = (logLine: string) => {
                console.log('onLog: ' + logLine)
                emit.next(logLine)
            }

            // trigger `onAdd()` when `add` is triggered in our event emitter
            robotEE.on('log', onLog);

            // unsubscribe function when client disconnects or stops subscribing
            return () => {
                console.log('onLog destroyed')
                robotEE.off('log', onLog);
            };

        })
    }),

  log: publicProcedure.input(z.string()).mutation(async (opts) => {
    const logLine = opts.input

    console.log('log: ' + logLine)

    robotEE.emit('log', logLine)
    return logLine
  }),

  onCommand: publicProcedure.subscription(() => {
    return observable<string>((emit) => {

      console.log('onCommand has been created')
      const onCommand = (command: string) => {
        console.log('onCommand: ' + command)
        emit.next(command)
      }

      // trigger `onAdd()` when `add` is triggered in our event emitter
      robotEE.on('command', onCommand);

      // unsubscribe function when client disconnects or stops subscribing
      return () => {
        console.log('onCommand destroyed')
        robotEE.off('command', onCommand);
      };

    })
  }),

  command: publicProcedure.input(z.string()).mutation(async (opts) => {
    const command = opts.input

    console.log('command: ' + command)

    robotEE.emit('command', command)
    return command
  })


})

// Merge routers together
export const appRouter = router({
    greeting: greetingRouter,
    post: postRouter,
    robot: robotRouter
});

export type AppRouter = typeof appRouter;

// http server
const { server, listen } = createHTTPServer({
    router: appRouter,
    createContext,
});

// ws server
const wss = new WebSocketServer({ server });
applyWSSHandler<AppRouter>({
    wss,
    router: appRouter,
    createContext,
});

// setInterval(() => {
//   console.log('Connected clients', wss.clients.size);
// }, 1000);
listen(2022);
console.log('Server Started')
