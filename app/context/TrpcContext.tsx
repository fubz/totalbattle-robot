import type { ReactNode } from "react";
import { createContext, useContext } from "react";
import type {TRPC} from '../trpc.client'
import {trpc} from '../trpc.client'

type ProviderProps = {
    children: ReactNode;
};

export const TrpcContext = createContext<TRPC | undefined>(undefined);

export function useTrpc() {
    return useContext(TrpcContext);
}

export function TrpcProvider({ children }: ProviderProps) {
    return <TrpcContext.Provider value={trpc}>{children}</TrpcContext.Provider>;
}