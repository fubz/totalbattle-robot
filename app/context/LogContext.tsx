import type { ReactNode } from "react";
import {createContext, useContext, useEffect, useState} from "react";
import {useTrpc} from "~/context/TrpcContext";

type ProviderProps = {
    children: ReactNode;
};

export const LogContext = createContext<string>('');

export function useLog() {
    return useContext(LogContext);
}

export function LogProvider({ children }: ProviderProps) {
    const trpc = useTrpc()
    const [log, setLog] = useState('')

    useEffect(() => {
        if(trpc) {
            console.log('trpc is defined :)')

            const robotLogSubscription = trpc.robot.onLog.subscribe(undefined, {
                onData(data) {
                    // ^ note that `data` here is inferred
                    console.log('onLog', data);
                    setLog((prevState) => {
                        return `${prevState}
${data}`
                    })
                },
                onError(err) {
                    console.error('error', err);
                },
            });

            return robotLogSubscription.unsubscribe
        }
    }, [trpc, setLog])




    return <LogContext.Provider value={log}>{children}</LogContext.Provider>;
}