#!/bin/bash

# Update and upgrade packages
sudo apt update
sudo apt upgrade -y

# Install required packages
sudo apt-get install -y libxtst-dev build-essential g++ cmake

# Install nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.4/install.sh | bash

# Load nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

# Install Node.js 16
nvm install 16
nvm use 16

echo "Setup complete."