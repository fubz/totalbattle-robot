const path = require("node:path")
const { screen } = require("@nut-tree/nut-js")
require("@nut-tree/template-matcher")

const templatesdir = path.join(process.cwd(), "templates")
console.log("Configuring Template Directory", templatesdir)
screen.config.resourceDirectory = templatesdir
screen.config.autoHighlight = true
screen.config.highlightDurationMs = 500
screen.config.confidence = 0.8
