import { z } from "zod";
import { initTRPC } from "@trpc/server";
import { fetchAdapter } from "@trpc/server/adapters/fetch";
import { io } from "socket.io";
import { createNamespace } from "socket.io/v3-adapter";
import { Context } from './context';

export const t = initTRPC.context<Context>().create();

export const robotRouter = t.router({
  commands: t
})

// Setup WebSocket server
const socketServer = io();
const robotNamespace = createNamespace("robot");
socketServer.adapter(robotNamespace);

// Listen to tRPC queries and broadcast events over WebSocket
robotAPI.events.on("*", (payload) => {
  robotNamespace.emit(payload.method, payload.input, payload.output);
});

// Expose tRPC and WebSocket servers
export { robotAPI, socketServer };
