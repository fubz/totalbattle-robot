const fs = require("fs")
const path = require("path")
const readline = require("readline")
const robot = require("./robot")
const Behavior = require("./behavior")

const behaviorsDir = path.join(__dirname, "behaviors")
const behaviorFiles = fs.readdirSync(behaviorsDir)

const commandQueue = []
const behaviors = {}

behaviorFiles.forEach((file) => {
  const name = file.substring(0, file.lastIndexOf(".js"))

  if (name) {
    const loadedBehavior = require(path.join(behaviorsDir, file))

    if (loadedBehavior instanceof Behavior) {
      behaviors[name] = loadedBehavior
    } else {
      console.log("Trashing bad behavior ", name)
      delete behaviors[name]
    }
  } else {
    console.log("discarding ", file)
  }
})

console.log("Behaviors")
console.log(behaviors)

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
})

async function run() {
  console.log("-- Total Battle Robot --")
  console.log("Commands:")
  console.log(Object.keys(behaviors))

  while (true) {
    const userInput = await askUser("Enter a command: ")
    await executeCommand(userInput)
  }
}

async function askUser(question) {
  return new Promise((resolve) => {
    rl.question(question, resolve)
  })
}

async function executeCommand(command) {
  command = command.trim()
  console.log(command)
  const nextBehavior = behaviors[command]
  console.log(nextBehavior)

  if (nextBehavior) {
    console.log(`Enqueuing ${command} behavior`)
    commandQueue.push(nextBehavior)
    processQueue()
  } else {
    console.log(
      `Unknown command. Available commands: ${Object.keys(behaviors)}`,
    )
  }
}

async function processQueue() {
  if (commandQueue.length === 0) {
    return
  }

  const nextBehavior = commandQueue.shift()
  if (!nextBehavior.running) {
    console.log(`Running ${nextBehavior} behavior`)
    nextBehavior.start()
  }

  processQueue()
}

run()
  .catch((error) => {
    console.error("An error occurred:", error)
  })
  .finally(() => {
    rl.close()
  })
