//src/RobotEE.ts
import { EventEmitter } from 'events';
import path from "path"
import fs from "fs"
import Behavior from "./Behavior"

const behaviorsDir = path.join(__dirname, "behaviors")
const behaviorFiles = fs.readdirSync(behaviorsDir)

class RobotEventEmitter extends EventEmitter {
    private commandQueue: string[] = ['idle'];
    private behaviors: { [key: string]: Behavior } = {}
    private running: boolean = false;

    constructor() {
        super();

        behaviorFiles.forEach((file) => {
            const name = file.substring(0, file.lastIndexOf(".ts"))

            if (name) {
                const loadedBehavior = require(path.join(behaviorsDir, file))

                if (loadedBehavior instanceof Behavior) {
                    this.behaviors[name] = loadedBehavior
                } else {
                    console.log("Trashing bad behavior ", name)
                    delete this.behaviors[name]
                }
            } else {
                console.log("discarding ", file)
            }
        })

        console.log("Behaviors")
        console.log(this.behaviors)

        this.on('command', this.handleCommand.bind(this));
    }

    enqueueCommand(command: string) {
        this.commandQueue.push(command);
        if (!this.running) {
            this.processQueue();
        }
    }

    private async processQueue() {
        if (this.commandQueue.length === 0) {
            this.running = false;
            this.emit('awaitingCommand');
            return;
        }

        this.running = true;
        const command = this.commandQueue.shift()!;
        this.emit('running', command);

        const behavior = this.behaviors[command]
        if(!behavior) {
            console.log('unknown behavior', command)
            this.processQueue()
        } else {
            try {
                await behavior.start(); // Wait for the behavior to complete
                this.emit('log', `Behavior completed: ${command}`);
            } catch (error) {
                console.error(`Error executing behavior ${command}:`, error);
                this.emit('log', `Error executing behavior ${command}: ${error}`);
            } finally {
                this.emit('completed', command);
                this.processQueue(); // Move to the next command in the queue
            }
        }
    }

    private handleCommand(command: string) {
        this.enqueueCommand(command);
    }
}

export default RobotEventEmitter;
