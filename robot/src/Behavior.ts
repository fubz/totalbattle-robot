class Behavior {
  private running: boolean
  constructor() {
    this.running = false
    Behavior.activeBehaviors.push(this)
  }

  static activeBehaviors: Behavior[] = []

  async start() {
    if (!this.running) {
      this.running = true
      await this.run()
    }
  }

  stop() {
    this.running = false
  }

  async run() {
    throw new Error("Subclasses must implement the run method")
  }
}

export default Behavior
