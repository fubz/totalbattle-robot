import Behavior from  "../Behavior"
import { Button, centerOf, imageResource, mouse, screen } from "@nut-tree/nut-js"


class Open extends Behavior {
  async run() {
    try {
      const region_help = await screen.find(imageResource("open.png"))

      await mouse.move(centerOf(region_help))
      await mouse.click(Button.LEFT)
    } catch (error) {
      console.log(error)
    }

    this.stop()
  }
}

module.exports = new Open()
