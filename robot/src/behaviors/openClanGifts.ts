import Behavior from "../Behavior"

import { Button, centerOf, imageResource, mouse, screen } from "@nut-tree/nut-js"



const clan_menu = imageResource("clan_menu.png")
const whole_clan_menu = imageResource("my_clan_whole_menu.png")
const clan_gifts = imageResource("clan_gifts.png")
const open = imageResource("open.png")

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

class Open extends Behavior {
  async run() {
    try {
      // Open Clan Menu
      let region = await screen.find(clan_menu, { confidence: 0.7 })
      await mouse.setPosition(await centerOf(region))
      await mouse.click(Button.LEFT)
      console.log("Clicked clan Menu")

      // Click on Gifts
      // region = await screen.find(whole_clan_menu, { confidence: 0.8 })
      // await screen.highlight(region)
      // console.log("found clan menu")

      await delay(2000) /// waiting 1 second.

      region = await screen.find(clan_gifts, {
        confidence: 0.85
      })
      await screen.highlight(region)
      await mouse.setPosition(await centerOf(region))
      await mouse.click(Button.LEFT)
      console.log("Clicked Gifts Menu")

      await delay(1000) /// waiting 1 second.

      while (this.running) {
        region = await screen.find(open, { confidence: 0.75 })
        await mouse.setPosition(await centerOf(region))
        await mouse.click(Button.LEFT)
        console.log("Clicked Open")
      }
    } catch (error) {
      console.log(error)
    }

    this.stop()
  }
}

module.exports = new Open()
