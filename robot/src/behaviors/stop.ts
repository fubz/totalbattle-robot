import Behavior from  "../Behavior"

class Stop extends Behavior {
  constructor() {
    super()
  }

  async run() {
    console.log("Stopping all behaviors")
    Behavior.activeBehaviors.forEach((behavior) => behavior.stop())
  }
}

module.exports = new Stop()
