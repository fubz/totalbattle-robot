import Behavior from  "../Behavior"
import { Button, centerOf, imageResource, mouse, screen } from "@nut-tree/nut-js"
require("@nut-tree/template-matcher"); // THIS IS NEW


const helpButton = imageResource("help.png")

class Help extends Behavior {
  async run() {
    try {
      const region_help = await screen.find(helpButton)
      console.log("Found Help:", region_help)

      await mouse.move(centerOf(region_help))
      await mouse.click(Button.LEFT)
    } catch (error) {
      console.log(error)
    }

    this.stop()
  }
}

module.exports = new Help()
