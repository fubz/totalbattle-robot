import Behavior from  "../Behavior"

class Exit extends Behavior {
  async run() {
    console.log("Exit")
    throw new Error("User Requested Exit")
  }
}

module.exports = new Exit()
