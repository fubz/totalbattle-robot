import Behavior from  "../Behavior"
const {
  mouse,
  centerOf,
  screen,
  imageResource,
  Button,
} = require("@nut-tree/nut-js")
const help = require("./help")

class HelpAll extends Behavior {
  async run() {
    while (this.running) {
      await help.run()
    }
  }
}

module.exports = new HelpAll()
