//src/robot.ts
import path from "node:path"
import { screen } from "@nut-tree/nut-js"
import {
    createTRPCProxyClient,
    createWSClient,
    httpLink,
    splitLink,
    wsLink,
} from '@trpc/client';
import type { AppRouter } from '../../app/trpc.server';

import AbortController from 'abort-controller';
import fetch from 'node-fetch';
import ws from 'ws';

import RobotEE from './RobotEE'
import type RobotEventEmitter from "./RobotEE"

// polyfill fetch & websocket
const globalAny = global as any;
globalAny.AbortController = AbortController;
globalAny.fetch = fetch;
globalAny.WebSocket = ws;
globalThis.WebSocket = ws as any;

// Configure foundational settings
const templatesdir = path.join(process.cwd(), "templates")
console.log("Configuring Template Directory", templatesdir)
screen.config.resourceDirectory = templatesdir
screen.config.autoHighlight = true
screen.config.highlightDurationMs = 500
screen.config.confidence = 0.8

const delay = (ms: number) => new Promise(resolve => setTimeout(resolve, ms))

const wsClient = createWSClient({
    url: `ws://${process.env.SERVER_HOST}`,
});
const trpc = createTRPCProxyClient<AppRouter>({
    links: [
        // call subscriptions through websockets and the rest over http
        splitLink({
            condition(op) {
                return op.type === 'subscription';
            },
            true: wsLink({
                client: wsClient,
            }),
            false: httpLink({
                url: `http://${process.env.SERVER_HOST}`,
            }),
        }),
    ],
});

// Define custom log functions
const customConsole = {
    log: (...args: any[]) => {
        const message = args.map(arg => String(arg)).join(' ');
        trpc.robot.log.mutate(message);

    },
    error: (...args: any[]) => {
        const message = args.map(arg => String(arg)).join(' ');
        trpc.robot.log.mutate(`[ERROR] ${message}`);
    },
};

// Replace console.log and console.error with the custom functions
console.log = customConsole.log;
console.error = customConsole.error;

// Set up the RobotEE instance and interact with it
export let robotEE: RobotEventEmitter | undefined = undefined;

robotEE = new RobotEE();

console.log("-- Total Battle Robot --");
console.log(Date.now());
console.log("Commands:");

robotEE.on('log', async (logLine) => {
    await trpc.robot.log.mutate(logLine);
});

const onCommand = trpc.robot.onCommand.subscribe(undefined, {
    onData(data) {
        // ^ note that `data` here is inferred
        console.log('onCommand', data);
        robotEE.emit('command', data);
    },
    onError(err) {
        console.error('error', err);
    },
});

// Start the robot behaviors or perform other robot-related operations here
// ...
